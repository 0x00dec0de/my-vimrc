set nocompatible

filetype off

set shell=sh

" set UTF-8 encoding

set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

call plug#begin('~/.vim/pack/plugins/start/')

Plug 'tpope/vim-fugitive'

" git support
Plug 'airblade/vim-gitgutter'

" go support for vim
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }

Plug 'exu/pgsql.vim'

" show marks
Plug 'kshenoy/vim-signature'

" auto change directory
Plug 'airblade/vim-rooter'

" add "end" for ruby etc
Plug 'tpope/vim-endwise'

Plug 'posva/vim-vue'

Plug 'digitaltoad/vim-pug'

" auto close pairs
Plug 'jiangmiao/auto-pairs'

Plug 'docunext/closetag.vim'

" fuzzyfinder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'mileszs/ack.vim'

" syntax checker
Plug 'w0rp/ale'

" show mached tag for html
Plug 'Valloric/MatchTagAlways'

" plugin for fast coding html
Plug 'mattn/emmet-vim'

" show indent line for functions
Plug 'Yggdroot/indentLine'

" show buffers tabs
Plug 'ap/vim-buftabline'

" A collection of language packs for Vim
" Plug 'sheerun/vim-polyglot'

Plug 'tpope/vim-unimpaired'

" autocomplete
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-gocode.vim'

" autocomplete
Plug 'shougo/neocomplete.vim'

" vim terminal integrations
Plug 'wincent/terminus'

" colorcheme
Plug 'altercation/vim-colors-solarized'
Plug 'romainl/flattened'

" snipets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" comment
Plug 'tyru/caw.vim'
Plug 'Shougo/context_filetype.vim'

call plug#end()

filetype plugin on


" let g:vue_disable_pre_processors=1



if exists('g:loaded_polyglot')
    let g:polyglot_disabled = ['go']
endif

" unisnipets
let g:UltiSnipsExpandTrigger='<tab>'
let g:UltiSnipsJumpForwardTrigger='<tab>'  " <c-b> default
let g:UltiSnipsJumpBackwardTrigger='<c-z>'


" AUTOCOMPLETE:
let g:asyncomplete_auto_popup = 1
let g:asyncomplete_smart_completion = 0
let g:asyncomplete_auto_popup = 1

setlocal omnifunc=go#complete#Complete

autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

call asyncomplete#register_source(asyncomplete#sources#gocode#get_source_options({
    \ 'name': 'gocode',
    \ 'whitelist': ['go'],
    \ 'completor': function('asyncomplete#sources#gocode#completor'),
    \ 'config': {
    \    'gocode_path': expand('~/go/bin/gocode')
    \  },
    \ }))


let g:sql_type_default = 'pgsql'

let g:vim_markdown_conceal = 0

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

set showbreak=↪\
" set list listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨
" set list listchars=tab:→\ ,eol:↲,nbsp:␣,trail:…,extends:⟩,precedes:⟨

" color for listchars
"hi NonText ctermfg=16 guifg=#4a4a59
"hi SpecialKey ctermfg=16 guifg=#4a4a59
"
" "  Unicode characters for various things
" set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·

" set wildmode=list:longest,full " List all options and complete
" set wildignore=*.class,*.o,*~,*.pyc,.git,node_modules  " Ignore certain files in tab-completion

set wildmenu
set wildmode=longest,list,full

set wildignore+=*.o,*.out,*.obj,.git,*.rbc,*.rbo,*.class,.svn,*.gem,*.pyc
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz
set wildignore+=*/vendor/gems/*,*/vendor/cache/*,*/.bundle/*,*/.sass-cache/*
set wildignore+=*/tmp/librarian/*,*/.vagrant/*,*/.kitchen/*,*/vendor/cookbooks/*
set wildignore+=*/tmp/cache/assets/*/sprockets/*,*/tmp/cache/assets/*/sass/*
set wildignore+=*.swp,*~,._*,*.jpg,*.png,*.gif,*.jpeg
set wildignore+=*/.DS_Store,*/tmp/*




" " Update and show lightline but only if it's visible (e.g., not in Goyo)
"autocmd User ALELint call s:MaybeUpdateLightline()
"function! s:MaybeUpdateLightline()
"  if exists('#lightline')
"    call lightline#update()
"  end
"endfunction

" important for alelint ( fix cursor showing on error line  )
let g:ale_echo_cursor = 0

" GOLANG
let g:go_auto_type_info = 1
"let g:go_fmt_command = "gofmt"
"let g:go_fmt_experimental = 1
let g:go_metalinter_autosave = 1
"let g:go_metalinter_autosave_enabled = ['vet', 'golint']
"let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']
"let g:go_term_enabled = 0
"let g:go_term_mode = "vertical"
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_interfaces = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_chan_whitespace_error = 1

"let g:go_list_type = "quickfix"
"let g:go_fmt_fail_silently = 1

autocmd QuitPre * if empty(&bt) | lclose | endif



au FileType go nmap <Leader>r <Plug>(go-run)
au FileType go nmap <Leader>b <Plug>(go-build)
au FileType go nmap <Leader>t <Plug>(go-test)
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>s <Plug>(go-implements)
au FileType go nmap <Leader>c <Plug>(go-coverage)
au FileType go nmap <Leader>e <Plug>(go-rename)
au FileType go nmap <Leader>gi <Plug>(go-imports)
au FileType go nmap <Leader>gI <Plug>(go-install)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <Leader>gb <Plug>(go-doc-browser)
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)


" GitGutter styling to use · instead of +/-
let g:gitgutter_sign_added = '•'
let g:gitgutter_sign_modified = '•'
let g:gitgutter_sign_removed = '•'
let g:gitgutter_sign_modified_removed = '•'

" Markdown could be more fruit salady
highlight link markdownH1 PreProc
highlight link markdownH2 PreProc
highlight link markdownLink Character
highlight link markdownBold String
highlight link markdownItalic Statement
highlight link markdownCode Delimiter
highlight link markdownCodeBlock Delimiter
highlight link markdownListMarker Todo


" Return to last edit position when opening files
augroup rememberlastpos

" swith buffer without saving
set hidden


" Ignore case when searching
set ignorecase

" Incremental search that show prtial maches
set incsearch

" Automatically switch search to case-sensitive when search query contains an uppercase letter.
set smartcase

" Convert tabs to space
set expandtab

filetype plugin indent on

" show numbers
set nu


if ! has("gui_running")
    set background=dark
    let g:solarized_visibility = "normal"
    let g:solarized_contrast = "normal"
    let g:solarized_termtrans = 1
    let g:solarized_termcolors=256
endif

" altercation/vim-colors-solarized
" romainl/flattened

colorscheme flattened_dark
"colorscheme solarized

let &colorcolumn=join(range(81,999),",")

" !!! better vim perfomance
" Don’t update screen during macro and script execution.
" set lazyredraw
let g:solarized_termcolors=256
" Limit the files searched for auto-completes
set complete-=i

" Search down into subfolders
" Provides tab-completion fo all file-related tasks
set path+=**

" Tweaks for bowsing
let g:netrw_banner=0 " disable annoying banner
let g:netrw_browse_split=4 " open in prior window
let g:netrw_altv=1 " open splits to the right
let g:netrw_liststyle=3 " thee view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'


set completeopt=longest,menuone,preview

set ttyfast

" No annoying sound on errors
set noerrorbells
set novisualbell

" always show cursor position
set ruler

nmap ; :Buffers<CR>
nmap f :Files<CR>
"nmap  :Tags<CR>


" smart tab's
set softtabstop=4 tabstop=4 shiftwidth=4 expandtab
set smarttab

" for html/rb files, 2 spaces
autocmd Filetype html setlocal ts=2 sw=2 expandtab
autocmd Filetype ruby setlocal ts=2 sw=2 expandtab

" for js/coffee/jade files, 4 spaces
autocmd Filetype javascript setlocal ts=4 sw=4 sts=0 expandtab
autocmd Filetype coffeescript setlocal ts=4 sw=4 sts=0 expandtab
autocmd Filetype jade setlocal ts=4 sw=4 sts=0 expandtab

" do not wrap lines
set nowrap

" Enable automatic indenting
set autoindent

" Turn on the 'smart' automatic indenting
set smartindent

" Инкриментный поиск, при поиске сразу выделяем найденое не ждём нажатия Enter для поиска
set incsearch

set matchtime=2
set matchpairs+=<:>

" Turn on the display the currently executing command in the lower right corner
" of the screen
set showcmd


" The size of the history to undo edits
set undolevels=10000

"centrolize cursor
set scrolloff=3

" history cmd line
set history=50

" use [RO] for [readonly] to save space in the message line:
set shortmess+=r


" Show the status bar is always
set laststatus=2

" sudo writeble
cmap w!! w !sudo tee % >/dev/null

" Offer auto-completion on the basis of already entered the register
set infercase

" Word Wrap on words, not letter by letter
set linebreak

" Display the first brace after entering the second
set showmatch

" Show a buffer name in the title of the terminal
set title

" Use dialogs instead of error messages
set confirm

" Ignoring case is a fun trick
set ignorecase

" And so is Artificial Intellegence!
set smartcase

" Remove any trailing whitespace that is in the file
autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif

"set linespace=15

" Show the status bar is always
set laststatus=2

" "HOT KEY"
nnoremap <C-h> :tabp<CR>
nnoremap <C-l> :tabn<CR>

" Better window navigation
nnoremap <S-j> <C-w>j
nnoremap <S-k> <C-w>k
nnoremap <S-h> <C-w>h
nnoremap <S-l> <C-w>l


" enable syntax highlignt
set hlsearch
" remove highlight after search
" nnoremap <esc><esc> :noh<return>
autocmd InsertEnter * :set nohlsearch
autocmd InsertLeave * :set hlsearch
"autocmd InsertEnter * :let @/=""
"autocmd InsertLeave * :let @/=""

" set relative number
autocmd InsertLeave * set relativenumber
autocmd InsertEnter * set norelativenumber

" set cursor
autocmd InsertEnter * set cul
autocmd InsertLeave * set nocul

" Now i use plugin 'terminus'
" Change cursor from block to line ( works only for kde terminal  )
" https://stackoverflow.com/questions/6488683/how-do-i-change-the-vim-cursor-in-insert-normal-mode
" let &t_SI = "\<Esc>]50;CursorShape=1\x7"
" let &t_EI = "\<Esc>]50;CursorShape=0\x7"


"function! LightlineLinterWarnings() abort
"  let l:counts = ale#statusline#Count(bufnr(''))
"  let l:all_errors = l:counts.error + l:counts.style_error
"  let l:all_non_errors = l:counts.total - l:all_errors
"  return l:counts.total == 0 ? '' : printf('%d ◆', all_non_errors)
"endfunction
"
"function! LightlineLinterErrors() abort
"  let l:counts = ale#statusline#Count(bufnr(''))
"  let l:all_errors = l:counts.error + l:counts.style_error
"  let l:all_non_errors = l:counts.total - l:all_errors
"  return l:counts.total == 0 ? '' : printf('%d ✗', all_errors)
"endfunction
"
"function! LightlineLinterOK() abort
"  let l:counts = ale#statusline#Count(bufnr(''))
"  let l:all_errors = l:counts.error + l:counts.style_error
"  let l:all_non_errors = l:counts.total - l:all_errors
"  return l:counts.total == 0 ? '✓ ' : ''
"endfunction
"
"
"
"set statusline=
"set statusline+=%7*\[%n]                                       "buffernr
"set statusline+=%1*\[%<%t]\                                    "File+path
"set statusline+=%2*\ %y\                                       "FileType
"set statusline+=%3*\ en:%{&enc}\ ff:%{&ff}\ f:%{&fenc}         "Encoding
"set statusline+=%4*\ Err:%{LightlineLinterErrors()}\               "Error
"set statusline+=%4*\ War:%{LightlineLinterWarnings()}\               "Error
"set statusline+=%4*\ Ok:%{LightlineLinterOK()}\               "Error
"set statusline+=%3*\ %{(&bomb?\",BOM\":\"\")}\                 "Encoding2
"set statusline+=%5*\ %=(ch:%3b\ hex:%2B)\                      "Char in
"set statusline+=%8*\ %=row:%l/%L\ (%03p%%)\                    "Rownumber/total (%)
"set statusline+=%9*\ col:%03c\                                 "Column
"set statusline+=%0*\ \ %m%r%w\ %P\ \                           "Modified? Readonly? Top/bot.


"   """""""""""""""""""""""
"   " status bar colors
"   au InsertEnter * hi statusline guifg=black guibg=#d7afff ctermfg=black ctermbg=magenta
"   au InsertLeave * hi statusline guifg=black guibg=#8fbfdc ctermfg=black ctermbg=cyan
"   hi statusline guifg=black guibg=#8fbfdc ctermfg=black ctermbg=cyan
"
"   " Status line
"   " default: set statusline=%f\ %h%w%m%r\ %=%(%l,%c%V\ %=\ %P%)
"
"


function! GitInfo()
  let git = fugitive#head()
  if git != ''
    return ' '.fugitive#head()
  else
    return ''
endfunction

let g:currentmode={
      \ 'n'  : 'NORMAL ',
      \ 'no' : 'N·Operator Pending ',
      \ 'v'  : 'VISUAL ',
      \ 'V'  : 'V·Line ',
      \ '' : 'V·Block ',
      \ 's'  : 'Select ',
      \ 'S'  : 'S·Line ',
      \ '' : 'S·Block ',
      \ 'i'  : 'INSERT ',
      \ 'R'  : 'REPLESE ',
      \ 'Rv' : 'V·Replace ',
      \ 'c'  : 'Command ',
      \ 'cv' : 'Vim Ex ',
      \ 'ce' : 'Ex ',
      \ 'r'  : 'Prompt ',
      \ 'rm' : 'More ',
      \ 'r?' : 'Confirm ',
      \ '!'  : 'Shell ',
      \ 't'  : 'Terminal '
      \}

set laststatus=2
set noshowmode
set statusline=

set statusline+=%0*\ %{toupper(g:currentmode[mode()])}\    " The current mode
set statusline+=%0*│                                       " Separator
set statusline+=%0*\ %n\                                   " Buffer number
set statusline+=%0*│                                       " Separator
set statusline+=%0*\ %{GitInfo()}\                         " Git Branch name
"set statusline+=%1*\ %<%F%m%r%h%w\                        " File path, modified, readonly, helpfile, preview
set statusline+=%1*\ %<%m%r%h%w\                           " Modified, readonly, helpfile, preview
set statusline+=%3*│                                       " Separator
set statusline+=%2*\ %Y\                                   " FileType
set statusline+=%3*│                                       " Separator
set statusline+=%2*\ %{''.(&fenc!=''?&fenc:&enc).''}       " Encoding
set statusline+=(%{&ff})                                   " FileFormat (dos/unix..)
set statusline+=%=                                         " Right Side
set statusline+=%2*\ c:%02v\                               " Colomn number
set statusline+=%3*│                                       " Separator
set statusline+=%1*\ l:%02l/%L(%3p%%)\                     " Line number / total lines, percentage of document


au InsertEnter * hi statusline guifg=black guibg=#d7afff ctermfg=black ctermbg=magenta
au InsertLeave * hi statusline guifg=black guibg=#8fbfdc ctermfg=black ctermbg=cyan
hi statusline guifg=black guibg=#8fbfdc ctermfg=black ctermbg=cyan

hi User1 ctermfg=007 ctermbg=239 guibg=#4e4e4e guifg=#adadad
hi User2 ctermfg=007 ctermbg=236 guibg=#303030 guifg=#adadad
hi User3 ctermfg=236 ctermbg=236 guibg=#303030 guifg=#303030
hi User4 ctermfg=239 ctermbg=239 guibg=#4e4e4e guifg=#4e4e4e



"   """""""""""""""""""""""





"   "  Dynamically getting the fg/bg colors from the current colorscheme, returns hex which is enough for me to use in Neovim
"   " Needs to figure out how to return cterm values too
"   let fgcolor=synIDattr(synIDtrans(hlID("Normal")), "fg", "gui")
"   let bgcolor=synIDattr(synIDtrans(hlID("Normal")), "bg", "gui")
"
"   " Tabline/Buffer line
"   set showtabline=2
"   set tabline="%1T"
"   " reverse hybrid tabline colors
"   if g:colors_name == 'hybrid'
"     highlight TabLineFill cterm=none gui=none
"     highlight TabLine cterm=none gui=none
"     " This doesn't work, odd!
"     " highlight TabLineSel ctermfg=black ctermfg=white guibg=fgcolor guifg=bgcolor
"     highlight TabLineSel ctermfg=black ctermfg=white guibg=#c5c8c6 guifg=#1d1f21
"     highlight BufTabLineActive cterm=none gui=none
"   endif
"
"   " Statusline
"   " https://github.com/Greduan/dotfiles/blob/76e16dd8a04501db29989824af512c453550591d/vim/after/plugin/statusline.vim
"
"   let g:currentmode={
"         \ 'n'  : 'N ',
"         \ 'no' : 'N·Operator Pending ',
"         \ 'v'  : 'V ',
"         \ 'V'  : 'V·Line ',
"         \ '' : 'V·Block ',
"         \ 's'  : 'Select ',
"         \ 'S'  : 'S·Line ',
"         \ '' : 'S·Block ',
"         \ 'i'  : 'I ',
"         \ 'R'  : 'R ',
"         \ 'Rv' : 'V·Replace ',
"         \ 'c'  : 'Command ',
"         \ 'cv' : 'Vim Ex ',
"         \ 'ce' : 'Ex ',
"         \ 'r'  : 'Prompt ',
"         \ 'rm' : 'More ',
"         \ 'r?' : 'Confirm ',
"         \ '!'  : 'Shell ',
"         \ 't'  : 'Terminal '
"         \}
"
"   " Automatically change the statusline color depending on mode
"   function! ChangeStatuslineColor()
"     if (mode() =~# '\v(n|no)')
"       exe ''
"     elseif (mode() =~# '\v(v|V)' || g:currentmode[mode()] ==# 'V·Block' || get(g:currentmode, mode(), '') ==# 't')
"       exe ''
"     elseif (mode() ==# 'i')
"       exe ''
"     else
"       exe ''
"     endif
"     return ''
"   endfunction
"
"   " Find out current buffer's size and output it.
"   function! FileSize()
"     let bytes = getfsize(expand('%:p'))
"     if (bytes >= 1024)
"       let kbytes = bytes / 1024
"     endif
"     if (exists('kbytes') && kbytes >= 1000)
"       let mbytes = kbytes / 1000
"     endif
"
"     if bytes <= 0
"       return '0'
"     endif
"
"     if (exists('mbytes'))
"       return mbytes . 'MB '
"     elseif (exists('kbytes'))
"       return kbytes . 'KB '
"     else
"       return bytes . 'B '
"     endif
"   endfunction
"
"   function! ReadOnly()
"     if &readonly || !&modifiable
"       return ''
"     else
"       return ''
"   endfunction
"
"   function! GitInfo()
"     let git = fugitive#head()
"     if git != ''
"       return ' '.fugitive#head()
"     else
"       return ''
"   endfunction
"
"   " http://stackoverflow.com/a/10416234/213124
"   set laststatus=2
"   set statusline=
"   set statusline+=%{ChangeStatuslineColor()}               " Changing the statusline color
"   set statusline+=%0*\ %{toupper(g:currentmode[mode()])}   " Current mode
"   set statusline+=%8*\ [%n]                                " buffernr
"   set statusline+=%8*\ %{GitInfo()}                        " Git Branch name
"   set statusline+=%8*\ %<%F\ %{ReadOnly()}\ %m\ %w\        " File+path
"   set statusline+=%#warningmsg#
"   " set statusline+=%{SyntasticStatuslineFlag()}             " Syntastic errors
"   set statusline+=%*
"   set statusline+=%9*\ %=                                  " Space
"   set statusline+=%8*\ %y\                                 " FileType
"   set statusline+=%7*\ %{(&fenc!=''?&fenc:&enc)}\[%{&ff}]\ " Encoding & Fileformat
"   set statusline+=%8*\ %-3(%{FileSize()}%)                 " File size
"   set statusline+=%0*\ %3p%%\ \ %l:\ %3c\                 " Rownumber/total (%)
"



"""""""""""""""""""""""
map ё `
map й q
map ц w
map у e
map к r
map е t
map н y
map г u
map ш i
map щ o
map з p
map х [
map ъ ]
map ф a
map ы s
map в d
map а f
map п g
map р h
map о j
map л k
map д l
map ж ;
map э '
map я z
map ч x
map с c
map м v
map и b
map т n
map ь m
map б ,
map ю .
"map . /

map Ё ~
map Й Q
map Ц W
map У E
map К R
map Е T
map Н Y
map Г U
map Ш I
map Щ O
map З P
map Х {
map Ъ }
map Ф A
map Ы S
map В D
map А F
map П G
map Р H
map О J
map Л K
map Д L
map Ж :
map Э "
map Я Z
map Ч X
map С C
map М V
map И B
map Т N
map Ь M
map Б <
map Ю >
" map , ?

cmap цй wq
cmap ц  w

" Use the mouse, but not in insert mode
silent! set mouse=nvc


if has("gui_running")
" select gorizontal cursor line
set cursorline

" select vertical cursor line
set cursorcolumn

" set guifont=Ubuntu\ Mono\ Regular\ 14
set guifont=Noto\ Mono\ Regular\ 15

" remove highlight
nnoremap <esc> :noh<return><esc>

" remove startup text
   set shortmess+=tToOI
 " set guioptions-=e " включаем none-gui табы
   set guioptions-=r " отключаем правый scrollbar
   set guioptions-=R " отключаем правый scrollbar при вертикальном разделении окна
   set guioptions-=b " отключаем нижний scrollbar
   set guioptions-=l " отключаем левый scrollbar
   set guioptions-=L " отключаем левый scrollbar при вертикальном разделении окна
   set guioptions-=T " отключаем панель инструментов
   set guioptions-=m " отключаем меню

   set wildmenu
   set wcm=<Tab>
   menu Encoding.koi8-r :set enc=koi8-r<CR> :e ++enc=koi8-r ++ff=unix<CR>
   menu Encoding.windows-1251 :set enc=cp1251<CR> :e ++enc=cp1251 ++ff=dos<CR>
   menu Encoding.cp866 :set enc=8bit-cp866<CR> :e ++enc=cp866 ++ff=dos<CR>
   menu Encoding.utf-8 :set enc=utf8<CR> :e ++enc=utf8 <CR>
   menu Encoding.koi8-u :set enc=koi8-u<CR> :e ++enc=koi8-u ++ff=unix<CR>
   " map <S-F10> :emenu Encoding.<TAB>

   set wildmenu
   set wcm=<Tab>
   menu Set\ Spell.ru  :set spl=ru spell<CR>
   menu Set\ Spell.en  :set spl=en spell<CR>
   menu Set\ Spell.ua  :set spl=ua spell<CR>
   menu Set\ Spell.ru_en  :set spl=ru,en spell<CR>
   menu Set\ Spell.off :set nospell<CR>
   menu Set\ Spell.next ]s
   menu Set\ Spell.prev [s
   menu Set\ Spell.word\ good zg
   menu Set\ Spell.word\ wrong zw
   menu Set\ Spell.word\ ignore zG

  " Colors (adapted from ligh2011.vim):
  hi User1 guifg=#ffdad8  guibg=#880c0e
  hi User2 guifg=#000000  guibg=#F4905C
  hi User3 guifg=#292b00  guibg=#f4f597
  hi User4 guifg=#112605  guibg=#aefe7B
  hi User5 guifg=#051d00  guibg=#7dcc7d
  hi User7 guifg=#ffffff  guibg=#880c0e gui=bold
  hi User8 guifg=#ffffff  guibg=#5b7fbb
  hi User9 guifg=#ffffff  guibg=#810085
  hi User0 guifg=#ffffff  guibg=#094afe

endif
